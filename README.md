# maodv - Enhanced-Ant-AODV

## Enhanced-Ant-AODV for optimal route selection in mobile ad-hoc network
**Nama    :**  Ilham Muhammad Misbahuddin<br>
**NRP     :**  05111540000088<br>
**Kelas   :**  Jaringan Nirkabel<br>
<br>
**Konsep  :**<br>
- Memodifikasi AODV dengan menambahkan konsep Pheromone Count value dari ACO (Ant Colony Optimization) pada header paket RREQ dan RREP.
- Pheromone Count value adalah suatu nilai yang didapatkan dari perhitungan 4 faktornya, yaitu : kekuatan sinyal, kepadatan jalur, energi tersisa dan hop count.
- Menambahkan Recieved Signal Strength Metric value, Congestion Metric value, Residual Energy Metric value, Pheromone Count value pada routing table.
- Menambahkan nilai threshold dari kekuatan sinyal (SIGNAL_THR) dan energi tersisa (RE_THR).

**Fungsi  :**<br>
- AODV::recvRequest(Packet *p)