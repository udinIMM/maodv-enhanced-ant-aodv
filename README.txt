scen.tcl untuk aodv ori & modifikasi tidak berbeda, dapat menggunakan file yang sama

Ruang Lingkup scen.tcl
----------------------------------------
Ukuran simulasi	:  910 x 910
Jumlah node	:  200 node
Waktu simulasi	:  200 detik
Energi awal	:  100 Joule
Energi transmit	:  8 Watt
Energi receive	:  6 Watt
Energi idle	:  0 Watt
Jarak transmit	:  400 meter
Jarak receive	:  400 meter
Energi awal source node	:  200 Joule
Terdapat 12 node diam atau 6 pasang node sebagai node pengirim paket CBR 

Ruang Lingkup setdest.tcl
----------------------------------------
Node yang memiliki ID mulai dari 188 hingga 199 adalah node diam, karena membutuhkan 6 pasang node atau 12 node diam
Berikut posisi 12 node diam :
Posisi node 188 berada pada (0, 0)
Posisi node 189 berada pada (0, 300)
Posisi node 190 berada pada (0, 600)
Posisi node 191 berada pada (0, 900)
Posisi node 192 berada pada (300, 0)
Posisi node 193 berada pada (600, 0)
Posisi node 194 berada pada (300, 900)
Posisi node 195 berada pada (600, 900)
Posisi node 196 berada pada (900, 0)
Posisi node 197 berada pada (900, 300)
Posisi node 198 berada pada (900, 600)
Posisi node 199 berada pada (900, 900)

Ruang Lingkup cbr.tcl
----------------------------------------
Tipe paket	:  CBR
Jumlah koneksi	:  maksimal 6 koneksi (terdapat 6 pasang)
Ukuran paket	:  512 bytes
Interval paket	:  4 paket/detik
Berikut pasangan node pengirim :
Node 188 mengirim ke node 199 dengan waktu pengiriman pertama pada detik ke 2.5568388786897245
Node 189 mengirim ke node 198 dengan waktu pengiriman pertama pada detik ke 56.333118917575632
Node 190 mengirim ke node 197 dengan waktu pengiriman pertama pada detik ke 46.96568928983328
Node 191 mengirim ke node 196 dengan waktu pengiriman pertama pada detik ke 55.634230382570173
Node 192 mengirim ke node 195 dengan waktu pengiriman pertama pada detik ke 29.546173154165118
Node 193 mengirim ke node 194 dengan waktu pengiriman pertama pada detik ke 7.7030203154790309

Pembuat :
Ilham M. Misbahuddin
05111540000088

